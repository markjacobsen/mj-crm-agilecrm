package net.markjacobsen.crm;

import com.agilecrm.api.APIManager;

public class CrmBase {
	private static APIManager apiManager = null;
	
	public CrmBase() {
		if (apiManager == null) {
			throw new RuntimeException("AgileCRM not initialized properly");
		}
	}
	
	public static void init(String domain, String userEmail, String apiKey) {
		String baseUrl = "https://"+domain+".agilecrm.com/dev";
		try {
			apiManager = new APIManager(baseUrl, userEmail, apiKey);
		} catch (Exception e) {
			throw new RuntimeException("Error initializing API Manager");
		}
	}
	
	public static APIManager getInstance() {
		return apiManager;
	}
}
