package net.markjacobsen.crm;

import java.util.ArrayList;
import java.util.List;

import com.agilecrm.api.DealAPI;
import net.markjacobsen.crm.domain.Deal;

public class DealsImpl implements Deals {
	private DealAPI dealsApi = CrmBase.getInstance().getDealAPI();
	
	@Override
	public List<Deal> getDeals() {
		List<Deal> deals = new ArrayList<>();
		List<com.agilecrm.stubs.Deal> apiDeals = dealsApi.getDeals();
		for (com.agilecrm.stubs.Deal apiDeal : apiDeals) {
			Deal deal = new Deal();
			deal.setName(apiDeal.getName());
			deal.setDescription(apiDeal.getDescription());
			deal.setId(apiDeal.getId().toString());
			deals.add(deal);
		}
		return deals;
	}
	
	public void setDealStage(Deal deal, String stage) throws CrmException {
		com.agilecrm.stubs.Deal apiDeal = null;
		try {
			apiDeal = dealsApi.getDealByDealId(deal.getId());
			apiDeal.setMilestone(stage);
			dealsApi.updateDeal(apiDeal);
		} catch (Exception e) {
			throw new CrmException("Error setting deal stage");
		}
	}
}
