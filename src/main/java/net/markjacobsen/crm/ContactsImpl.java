package net.markjacobsen.crm;

import java.util.List;

import com.agilecrm.api.ContactAPI;
import com.agilecrm.stubs.ContactField;

import net.markjacobsen.crm.domain.Contact;

public class ContactsImpl implements Contacts {
	private ContactAPI contactApi = CrmBase.getInstance().getContactAPI();
	
	@Override
	public Contact getContactFromEmail(String email) throws CrmException {
		Contact contact = null;
		try {
			com.agilecrm.stubs.Contact apiContact = contactApi.getContactFromEmail(email);
			contact = new Contact();
			contact.setId(apiContact.getId().toString());
			List<ContactField> fields = apiContact.getProperties();
			for (ContactField field : fields) {
				if ("email".equals(field.getName())) {
					if ("home".equals(field.getSubtype())) {
						contact.setEmailHome(field.getValue());
					} else if ("work".equals(field.getSubtype())) {
						contact.setEmailWork(field.getValue());
					} else {
						contact.setEmailOther(field.getValue());
					}
				} else if ("first_name".equals(field.getName())) {
					contact.setFirstName(field.getValue());
				} else if ("last_name".equals(field.getName())) {
					contact.setLastName(field.getValue());
				} else if ("phone".equals(field.getName())) {
					if ("home".equals(field.getSubtype())) {
						contact.setPhoneHome(field.getValue());
					} else if ("work".equals(field.getSubtype())) {
						contact.setPhoneWork(field.getValue());
					} else {
						contact.setPhoneMobile(field.getValue());
					}
				} else if ("company".equals(field.getName())) {
					contact.setCompany(field.getValue());
				} else if ("title".equals(field.getName())) {
					contact.setTitle(field.getValue());
				} else {
					System.out.println(field.getName()+" = "+field.getValue());
				}
			}
		} catch (Exception e) {
			throw new CrmException("Error getting contact", e);
		}
		return contact;
	}

}
